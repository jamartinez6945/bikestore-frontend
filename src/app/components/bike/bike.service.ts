import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { IBike } from './bike';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BikeService {


  constructor(private http: HttpClient) { }

  public query(): Observable<IBike[]> {
    return this.http.get<IBike[]>(`${environment.END_POINT}/api/called`)
    .pipe(map( res => {
      return res;
  })); // end method query
}

public saveBike(bike: IBike): Observable<IBike> {
return this.http.post<IBike>(`${environment.END_POINT}/api/bike`, bike)
.pipe(map( res => {
  return res;
}));
}// end method saveBike

public getById(id: string): Observable<IBike> {
return this.http.get<IBike>(`${environment.END_POINT}/api/bike/${id}`)
.pipe(map( res => {
  return res;
}));
}

public update(bike: IBike): Observable<IBike>  {
  return this.http.put<IBike>(`${environment.END_POINT}/api/bike`, bike)
  .pipe(map( res => {
    return res;
  }));
}

}
