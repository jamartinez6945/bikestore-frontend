import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientListComponent } from './client-list/client-list.component';
import { ClientCreateComponent } from './client-create/client-create.component';
import { ClientUpdateComponent } from './client-update/client-update.component';
import { ClientViewComponent } from './client-view/client-view.component';


const routes: Routes = [
  {
    path: 'client-list',
    component: ClientListComponent
  },
  {
    path: 'client-create',
    component: ClientCreateComponent
  },
  {
    path: 'client-update',
    component: ClientUpdateComponent
  },
  {
    path: 'client-view',
    component: ClientViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
